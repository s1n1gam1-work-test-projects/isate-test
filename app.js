function factorial(n){
    let res = 1;

    if(n<0 || n>10){
        return new  Error("Передан неправильный параметр")
    } else{
        for(let i=1;i<n;i++){
            res=res*(i+1)
        }

        return res
    }
    
} 


function isPrime(n){
    let flag = true;

    if(n>1000){
        return new Error('Слишком большие значения для вычисления')
    } else{
        for(i=2;i<n;i++){
            if(n%i==0){
                flag=false;
                break
            }

        }
    }

    return flag;

}


function fib(n){
    if(n>1000){
        return new Error('Слишком большие значения для вычисления')
    } else{
        let prev = 0;
        let current =1;

        for(i=2;i<n;i++){
            current+=prev;
            prev=current-prev

        }

        return current
    }
}


function customSort(arr){
    if(!Array.isArray(arr)){
        return new Error('Передан ошибочный тип для данной функции')
    } else{
        let arrlen = arr.length;

        for(let i=0;i<arrlen;i++){
            let buff = 0;
            for(let j=0;j<arrlen;j++){
                buff = arr[i]
                if(arr[i]<arr[j]){
                    arr[i]=arr[j]
                    arr[j]=buff
                }
            }   
        }

        return arr

    }

}


function customFilter(arr,conditionFunction){
    if(!Array.isArray(arr)){
        return new Error('Передан ошибочный тип для данной функции')
    } else{
        let res = [];

        arr.forEach((each)=>{
            if(conditionFunction(each)){
                res.push(each)
            }
        })

        return res;

    }
}

function customReduce(arr,conditionFunction,sum){
    if(!Array.isArray(arr)){
        return new Error('Передан ошибочный тип для данной функции')
    } else{
    
        arr.forEach((each)=>{
            sum=conditionFunction(sum,each)
        })

        return sum
    }
}

function customReverse(str){
    let strArr = str.split('');
    let res ='';

    for(let i=strArr.length-1;i>=0;i--){
        res+=strArr[i]
    }

    return res
}

function customIndexOf(element,arr){
    let elementIndex = null;
    
    arr.forEach((each,index)=>{
        if(each===element&&elementIndex===null){
            elementIndex=index;
        }
    })

    if(elementIndex!==null){
        return elementIndex
    } else{
        return -1
    }
    
}


function isSorted(arr){
    if(!Array.isArray(arr)){
        return new Error('Передан ошибочный тип для данной функции')
    } else{

        let buff=arr.filter((each)=>{return true});
        let res = arr.sort((a,b)=>a-b)
    
        return equalsArray(res,buff)
    }
}


function isSorted2(arr){
    if(!Array.isArray(arr)){
        return new Error('Передан ошибочный тип для данной функции')
    } else{

        let buff=arr.filter((each)=>{return true});
        let res = customSort(arr)
    
        return equalsArray(res,buff)
    }
}


function equalsArray(arr1,arr2){
    let equals = true;

    arr1.forEach((each,index)=>{
        if(each!==arr2[index]){
            equals=false
        }
    })

    return equals
}
