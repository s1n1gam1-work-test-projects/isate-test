# isate

## sql задачи
* Напишите запрос, который может выдать вам все заказы со значениями суммы выше $1,000
```sql
SELECT * FROM orders WHERE snum > 1000
```

* Напишите запрос к таблице Заказчиков, чей вывод включит всех заказчиков с оценкой =&lt; 100, если они не находятся в Риме.
```sql
SELECT * FROM customer WHERE rating <= 100 AND city <> 'Rome'
```

* Напишите запрос, который выбрал бы наименьшую сумму для каждого заказчика.
```sql

```

* Напишите запрос, использующий оператор EXISTS, который выберет продавцов, заказчики которых находятся в тех же городах, но не обслуживаются продавцами.
```sql

```


## js задачи ( простые )
* Реализуйте функцию factorial(), которая возвращает факториал переданного ей числа.
```javascript
function factorial(n){
    let res = 1;

    if(n<0 || n>10){
        return new  Error("Передан неправильный параметр")
    } else{
        for(let i=1;i<n;i++){
            res=res*(i+1)
        }

        return res
    }
    
} 
```

* Реализуйте функцию isPrime(), которая возвращает true или false, указывая, является ли переданное ей число простым.
```javascript
function isPrime(n){
    let flag = true;

    if(n>1000){
        return new Error('Слишком большие значения для вычисления')
    } else{
        for(i=2;i<n;i++){
            if(n%i==0){
                flag=false;
                break
            }

        }
    }

    return flag;

}
```

* Реализуйте функцию fib(), возвращающую n-ное число Фибоначчи.
```javascript
function fib(n){
    if(n>1000){
        return new Error('Слишком большие значения для вычисления')
    } else{
        let prev = 0;
        let current =1;

        for(i=2;i<n;i++){
            current+=prev;
            prev=current-prev

        }

        return current
    }
}
```

* Напишите любой алгоритм сортировки массива. Например, есть массив [2,5,3,7,1]. На выходе должен получиться массив [1,2,3,5,7]. Примечание: нельзя писать просто Array.sort(). Нужно сделать полную реализацию.
```javascript
function customSort(arr){
    if(!Array.isArray(arr)){
        return new Error('Передан ошибочный тип для данной функции')
    } else{
        let arrlen = arr.length;

        for(let i=0;i<arrlen;i++){
            let buff = 0;
            for(let j=0;j<arrlen;j++){
                buff = arr[i]
                if(arr[i]<arr[j]){
                    arr[i]=arr[j]
                    arr[j]=buff
                }
            }   
        }

        return arr

    }

}
```


## js задачи ( сложные )
* Создайте собственную реализацию функции  filter() . filter([1, 2, 3, 4], n => n < 3)    // [1, 2]
```javascript
function customFilter(arr,conditionFunction){
    if(!Array.isArray(arr)){
        return new Error('Передан ошибочный тип для данной функции')
    } else{
        let res = [];

        arr.forEach((each)=>{
            if(conditionFunction(each)){
                res.push(each)
            }
        })

        return res;

    }
}
```

* Создайте собственную реализацию функции  reduce() . reduce([1, 2, 3, 4], (a, b) => a + b, 0) // 10
```javascript
function customReduce(arr,conditionFunction,sum){
    if(!Array.isArray(arr)){
        return new Error('Передан ошибочный тип для данной функции')
    } else{
    
        arr.forEach((each)=>{
            sum=conditionFunction(sum,each)
        })

        return sum
    }
}
```

* Реализуйте функцию reverse(), которая обращает порядок следования символов переданной ей строки. Не пользуйтесь встроенной функцией  reverse() . reverse('abcdef') // 'fedcba'
```javascript
function customReverse(str){
    let strArr = str.split('');
    let res ='';

    for(let i=strArr.length-1;i>=0;i--){
        res+=strArr[i]
    }

    return res
}
```

* Создайте собственную реализацию функции  indexOf()  для массивов.
```javascript
function customIndexOf(element,arr){
    let elementIndex = null;
    
    arr.forEach((each,index)=>{
        if(each===element&&elementIndex===null){
            elementIndex=index;
        }
    })

    if(elementIndex!==null){
        return elementIndex
    } else{
        return -1
    }
    
```

* Реализуйте функцию isSorted(), которая возвращает true или false в зависимости о того, отсортирован ли переданный ей числовой массив.
```javascript
function isSorted(arr){
    if(!Array.isArray(arr)){
        return new Error('Передан ошибочный тип для данной функции')
    } else{

        let buff=arr.filter((each)=>{return true});
        let res = arr.sort((a,b)=>a-b)
    
        return equalsArray(res,buff)
    }
}

function equalsArray(arr1,arr2){
    let equals = true;

    arr1.forEach((each,index)=>{
        if(each!==arr2[index]){
            equals=false
        }
    })

    return equals
}
```

